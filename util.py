"""
	File: util.py
	Author: Temet <temet@teknik.io> & Prawnzy <prawnzy@teknik.io>
	Description:
		A script that provides a set of functions to facilitate twitter 
		feeds by location. If ran in python as a standalone, it'll prompt
		you for a location/address and stream tweets 
"""
import tweepy
import details #include your tokens in details.py
from geopy.geocoders import Nominatim
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from time import sleep

""" 
	Tokens for access to the Twitter Oauth API
"""
consumer_key  = details.consumer_token
consumer_secret = details.consumer_secret

access_token    = details.access_token
access_token_secret   = details.access_secret

"""
	Creates API object used by tweepy
	and returns it
"""
def get_twitter_API(consumer_t, consumer_s, access_t, access_s):

	#Auth into Tweepy
	auth = tweepy.OAuthHandler(consumer_t, consumer_s)
	auth.set_access_token(access_t, access_s)

	#Todo: handle exceptions
	api = tweepy.API(auth)
	return api
	
"""
	def get_by_loc(lat lon, radius, api):
	- lat: lattitude of area
	- lon: longitude of area
	- radius: the radius of a circle extending outwards from (lat,lon). 
	   - Also include the unit of measure (mi, km).
	- api - A properly activated tweepy.API() object with access tokens embeded. 
"""	
def get_by_loc(lat, lon, radius, api):
	res = api.search(q="*", geocode=str(lat)+","+str(lon)+","+radius)
	
	#buffer: 
	buf = []
	for tweet in res:
		#Format the string for easer viewing
		buf.append("@%s : %s" % (tweet.user.screen_name, tweet.text))
	
	return buf
"""
	def get_loc_by_address(addr)
	- addr: a string specifying a name of a loation/address
	- returns a lat,lon list specifying the coordinate of that location
"""
def get_loc_by_address(addr):
	geolocator = Nominatim()
	loc = geolocator.geocode(addr)
	
	#return a lat/lon pair
	return [loc.latitude, loc.longitude]
	
if __name__ == '__main__':

	banner = """
	-
	-	Temet's & Prawnzy's Twitter Stream Surveiller
	-		Visit us on #/g/punk on Rizon IRC
	-                  irc.rizon.net		
	"""
	
	#Present and configure
	print(banner)
	addr   = raw_input("Enter Location to Monitor: ")
	delay  = int(raw_input("How many seconds per query (recommended 250)"))
	radius = raw_input("Radius in miles to extend from point for status inlusion: (ex. 2mi)")
	loc         = get_loc_by_address(addr)
	tweetBuffer = []
	tweetapi = get_twitter_API(consumer_key, consumer_secret, access_token, access_token_secret)
	
	#Loop forever
	while True:
		tmpbuf = get_by_loc(loc[0], loc[1], radius, tweetapi)
		
		#- Loop through each one returned and only print/store
		#- unique values not already held in tweetBuffer
		for tweet in tmpbuf:
			
			if tweet not in tweetBuffer:
				print tweet
				tweetBuffer.append(tweet)
		#Now we sleep 
		sleep(delay)
