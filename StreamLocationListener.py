"""
	File: StreamLocationListener.py
	Author: temet <temet@teknik.io> & Prawnzy <prawnzy@teknik.io>
	Descripton:
		An extension of the StreamListener BaseClass that is able to 
		access Twitter's Live Tweet Streams. 
		
		This class is usable in the context of location based searches. 

"""

import tweepy
import util
import details2

class StreamLocationListener(tweepy.StreamListener):
	
	#On status - called when a new status is received. 
	def on_status(self, status):
		
		#Save everything to a file log. 
		with open("/home/trusty/tweet.log","a") as log:
			log.write(status.text.encode("utf-8") + "\n")
			print status.text.encode("utf-8")
	
	def on_error(self, status_code):
		#Back off
		if status_code == 420:
			return False
		else:
			return True

if __name__ == "__main__":

	#Set up stream.
	api = util.get_twitter_API(details2.consumer_token, details2.consumer_secret, details2.access_token, details2.access_secret)
	myListener = StreamLocationListener()
	myStream = tweepy.Stream(auth = api.auth, listener=myListener)
	
	#print banner
	print("-" * 30)
	print("-  Temet's TwitterStream Listener       -")
	print("-" * 30)
	
	"""
	isLoc   = raw_input("Should we include a location? (y/n): ")
	gps     = None
	
	#if isLoc included, then retrieve gps
	if isLoc in "y":
		loc = raw_input("Location to track: ")
		gps = util.get_loc_by_address(loc)
		myStream.filter(locations = gps, async=True)
	else:
	"""
	myStream.filter(track=[raw_input("Keyword to track: ")], async=True)
