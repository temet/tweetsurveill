#Tweet Surveil#
_Temet <temet@teknik.io>_
***

_TweetSurveil_ attempts at chronicaling events that happen in real time 
and interpret them in a context-aware manner. So far the most you can 
achieve is to narrow a twitter feed down to a geographic location. 

##What's Next?##

The next step is to create a simple intelligence regarding what is 
considered a "normal" situation, and when something apparently noteworthy
does. And afterwhich, when an emerging situation is in play, _scrutinizing_
the situation with much more of a watchful eye. 

The intended featureset for this piece of software is expected to include:

+ _Watch Words_: If a certain phrase or set of words that determine severity of a situation.
+ _Trending Topics_: follow trends and determine the subject of the trend.
+ _User Intervention_: Using a trigger tag (ex. #shooting #activeshooter) to denote a situation.
+ _External Resources_: Using RSS feeds to signal a geographically relevant event. 
+ _Dynamic Mapping_ : Using _Google Maps_ or similar mapping software to map out events.
+ _GUI_: Provide a graphical interface to ease the use of this software
+ _SQL Database Integration_: Collecting vital information in an SQL database to speed up data comprehension.

##Why would that help?##

Our social networks provide us with a bevy of useful information. Even
without trying to circumvent what we consider a normal level of privacy, 
we can afford ourselves a level of scrutiny to our surrounding environment.
And in lieu of the Paris attacks, might save lives in the future. 

##Why are you doing this?##

Sitting from my desk and watching everything unfold; I was overcome by a
great sense of helplessness. This is my meager answer to those that seek
to harm our general society as well as the individuals we cherish in that
society. 

##How Do I use this?##

This script uses several dependencies and is written (and ran) in a Python
environment. Before utilizing this piece of software, install `tweepy` and `geopy`.

These commands will install these packages in Linux:

1. `pip install tweepy`
2. `pip install geopy`

And to run the script, travel to the directory where the script resides and run 
the following command in your favorite terminal/shell:

`python util.py`

Follow the prompts to configure the paramaters of the script. 

*_Note_*: 
>You must have a valid twitter account and generate an application token.
>Visit the site `http://apps.twitter.com` to complete the process. From within 
>the details.py source code, there are appropriate fields to input the tokens
>that Twitter gives you. Afterwhich, you are ready to use it. 
