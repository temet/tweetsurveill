"""
	File: StreamListener.py
	Author: temet & Prawnzy @ teknik.io
	Details:
		Defines a Class for streaming Twitter Tweets
"""
import tweepy

class StreamListener(tweepy.StreamListener):
	
	"""
	    def on_status
	    - Handles a status stream
	    - Should include geotagging here
	"""
	   def on_status(self, status):
		print status.text
#~end Class StreamListener
